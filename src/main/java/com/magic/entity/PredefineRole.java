package com.magic.entity;
// Generated Jan 20, 2016 4:45:40 PM by Hibernate Tools 4.3.1.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * PredefineRole generated by hbm2java
 */
@Entity
@Table(name = "predefine_role")
public class PredefineRole implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6984442720027639426L;
	private Long id;
	private String name;
	private Set<Role> roles = new HashSet<Role>(0);

	public PredefineRole() {
	}

	public PredefineRole(String name) {
		this.name = name;
	}

	public PredefineRole(String name, Set<Role> roles, Set<RoleUser> roleUsers) {
		this.name = name;
		this.roles = roles;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "predefineRole")
	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}
