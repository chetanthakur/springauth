package com.magic.platform.ecom.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AdminUserAuthSuccessHandler implements AuthenticationSuccessHandler {
	final Logger LOGGER = Logger.getLogger(AdminUserAuthSuccessHandler.class);

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	        Authentication authentication) throws IOException, ServletException {

		if (authentication.getPrincipal() instanceof MagicUserDetails) {
			MagicUserDetails magicUserDetails = (MagicUserDetails) authentication.getPrincipal();

			if (magicUserDetails.getPreDefineRole().equalsIgnoreCase("student")
			        || magicUserDetails.getPreDefineRole().equalsIgnoreCase("ind_student")) {
				response.sendRedirect("/learner/dashboard.htm");
			} else {
				response.sendRedirect("/admin/dashboard.htm");
			}
		}
	}
}
