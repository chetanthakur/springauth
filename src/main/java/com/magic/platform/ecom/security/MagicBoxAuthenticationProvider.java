package com.magic.platform.ecom.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.magic.entity.PermissionStatus;
import com.magic.entity.Role;
import com.magic.entity.RoleUser;
import com.magic.entity.User;
import com.magic.helper.PermissionUserVo;
import com.magic.platform.manager.UserManager;

public class MagicBoxAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private UserManager userManager;

	@Override
	protected void additionalAuthenticationChecks(UserDetails arg0, UsernamePasswordAuthenticationToken arg1)
	        throws AuthenticationException {
		System.out.println("inside additionalAuthenticationChecks");
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	protected UserDetails retrieveUser(String userName, UsernamePasswordAuthenticationToken authentication)
	        throws AuthenticationException {
		System.out.println("inside retrieveUser");
		User users = userManager.authenticateUser(userName, authentication.getCredentials().toString());
		String preDefineRole = null;
		if (users != null) {
			List<String> rolePermission = userManager
			        .getRolePermission(users.getRoleUsers().iterator().next().getRole().getId());
			preDefineRole = users.getRoleUsers().iterator().next().getRole().getPredefineRole().getName();

			List<PermissionUserVo> permissionUser = userManager.getPermissionUser(users.getId());

			for (PermissionUserVo permissionUserVo : permissionUser) {
				if (permissionUserVo.getPermissionStatus() == PermissionStatus.REVOKE
				        && rolePermission.contains(permissionUserVo.getName())) {
					rolePermission.remove(permissionUserVo.getName());
				}
				if (permissionUserVo.getPermissionStatus() == PermissionStatus.GRANT
				        && rolePermission.contains(permissionUserVo.getName())) {
					rolePermission.add(permissionUserVo.getName());
				}
			}

			if (rolePermission != null) {
				Set<RoleUser> roleUserSet = new HashSet<RoleUser>();
				for (String permission : rolePermission) {
					RoleUser roleUser = new RoleUser();
					Role role = new Role();
					role.setName(permission);
					roleUser.setRole(role);
					roleUserSet.add(roleUser);
				}
				users.getRoleUsers().addAll(roleUserSet);
			}
		}
		return new MagicUserDetails(users, preDefineRole);
	}
}
