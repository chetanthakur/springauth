package com.magic.platform.ecom.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.magic.entity.User;

public class MagicUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3618000203047756967L;

	private User user;
	private String preDefineRole;

	public String getPreDefineRole() {
		return preDefineRole;
	}

	public User getUser() {
		return user;
	}

	public MagicUserDetails(User user, String preDefineRole) {
		this.user = user;
		this.preDefineRole = preDefineRole;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public boolean equals(Object rhs) {
		if (!(rhs instanceof MagicUserDetails) || (rhs == null)) {
			return false;
		}

		MagicUserDetails user = (MagicUserDetails) rhs;

		// We rely on constructor to guarantee any User has non-null
		// authorities
		if (!getAuthorities().equals(user.getAuthorities())) {
			return false;
		}

		// We rely on constructor to guarantee non-null username and password
		return (this.getUsername().equals(user.getUsername()));
	}

	@Override
	public int hashCode() {
		int code = 9792;

		if (this.getUsername() != null) {
			code = code * (this.getUsername().hashCode() % 7);
		}

		return code;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.user.getRoleUsers();
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

}
