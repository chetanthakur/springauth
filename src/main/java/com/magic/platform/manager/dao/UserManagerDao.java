package com.magic.platform.manager.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.magic.entity.PermissionUser;
import com.magic.entity.RolePermission;
import com.magic.entity.User;
import com.magic.helper.PermissionUserVo;

@Component
public class UserManagerDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public User authenticateUser(String userName, String password) {

		Session session = null;
		session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		if (userName != null) {
			criteria.add(Restrictions.eq("username", userName));
			criteria.add(Restrictions.eq("password", password));
		}
		return (User) criteria.uniqueResult();
	}

	@Transactional(readOnly = true)
	public List<String> getRolePermission(Long long1) {

		Session session = null;
		session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(RolePermission.class);
		if (long1 != null) {
			// criteria.createAlias("role", "roleAli");
			criteria.add(Restrictions.eq("role.id", long1));
		}
		criteria.createAlias("permission", "permissionAli");
		criteria.setProjection(Projections.groupProperty("permissionAli.name"));
		return criteria.list();
	}

	@Transactional(readOnly = true)
	public List<PermissionUserVo> getPermissionUser(Long id) {

		Session session = null;
		List<PermissionUserVo> permissionUserVoList = null;
		try {
			session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(PermissionUser.class);
			if (id != null) {
				criteria.add(Restrictions.eq("user.id", id));
			}
			criteria.createAlias("permission", "permissionAli");
			ProjectionList projList = Projections.projectionList();
			criteria.setProjection(projList.add(Projections.property("permissionAli.name"), "name")
			        .add(Projections.property("permissionStatus"), "permissionStatus"));

			criteria.setResultTransformer(Transformers.aliasToBean(PermissionUserVo.class));
			permissionUserVoList = criteria.list();
		} catch (HibernateException Ex) {
			Ex.printStackTrace();
		}
		return permissionUserVoList;
	}
}
