package com.magic.platform.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.magic.entity.User;
import com.magic.helper.PermissionUserVo;
import com.magic.platform.manager.dao.UserManagerDao;

// @Primary
public class UserManager {

	@Autowired
	private UserManagerDao userManagerDao;

	public User authenticateUser(String userName, String password) {
		return userManagerDao.authenticateUser(userName, password);
	}

	public List<String> getRolePermission(Long long1) {
		return userManagerDao.getRolePermission(long1);
	}

	public List<PermissionUserVo> getPermissionUser(Long id) {
		return userManagerDao.getPermissionUser(id);
	}

}
