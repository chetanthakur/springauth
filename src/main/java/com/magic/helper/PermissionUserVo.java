package com.magic.helper;

import com.magic.entity.PermissionStatus;

public class PermissionUserVo {

	private String name;
	private PermissionStatus permissionStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PermissionStatus getPermissionStatus() {
		return permissionStatus;
	}

	public void setPermissionStatus(PermissionStatus permissionStatus) {
		this.permissionStatus = permissionStatus;
	}

}
